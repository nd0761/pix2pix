import sys
import os
import wandb
import copy

import torch
import torch.nn as nn
import torchvision.transforms
from utils.config import TaskConfig

from utils.trainer import train
from utils.model.generator import UNet
from utils.dataset import FacadesDataset

from torch.utils.data import DataLoader
from torch.optim import AdamW
from torch.optim.lr_scheduler import OneCycleLR


def main_worker():
    print("set torch seed")
    config = TaskConfig()
    print(config.device)
    torch.manual_seed(config.torch_seed)

    print("initialize dataset")
    train_dataset = FacadesDataset(
        os.path.join(config.work_dir_dataset, config.dataset_name),
        torchvision.transforms.ToTensor(), torchvision.transforms.ToTensor(),
        split="train")
    val_dataset = FacadesDataset(
        os.path.join(config.work_dir_dataset, config.dataset_name),
        torchvision.transforms.ToTensor(), torchvision.transforms.ToTensor(),
        split="val"
    )
    test_dataset = FacadesDataset(
        os.path.join(config.work_dir_dataset, config.dataset_name),
        torchvision.transforms.ToTensor(), torchvision.transforms.ToTensor(),
        split="test"
    )

    print("initialize dataloader")

    train_loader = DataLoader(
        train_dataset,
        batch_size=config.batch_size
    )

    val_loader = DataLoader(
        val_dataset,
        batch_size=config.batch_size
    )

    test_loader = DataLoader(
        test_dataset,
        batch_size=config.batch_size
    )
    print("Train size:", len(train_dataset), len(train_loader))

    print("initialize model")
    model = nn.DataParallel(UNet()).to(config.device)
    model.to(config.device)
    print("model parameters:", sum(param.numel() for param in model.parameters()))

    print("initialize optimizer")
    opt = AdamW(
        model.parameters(),
        lr=config.learning_rate,
        weight_decay=config.weight_decay,
        betas=(0.9, 0.98), eps=1e-9
    )

    print("initialize scheduler")
    # if config.batch_limit != -1:
    #     scheduler = OneCycleLR(opt, steps_per_epoch=config.batch_limit,
    #                            epochs=config.num_epochs + 1,
    #                            max_lr=config.learning_rate,
    #                            pct_start=0.1
    #                            )
    # else:
    #     scheduler = OneCycleLR(opt, steps_per_epoch=len(train_loader),
    #                            epochs=config.num_epochs + 1,
    #                            max_lr=config.learning_rate,
    #                            pct_start=0.1
    #                            )

    print("initialize wandb")
    wandb_session = wandb.init(project=config.wandb_project, entity="nd0761")
    wandb.config = config.__dict__

    print("start train procedure")

    train(
        model, opt,
        train_loader, val_loader,
        scheduler=None,
        save_model=False, model_path=None,
        config=config, wandb_session=wandb_session
    )


if __name__ == "__main__":
    main_worker()
