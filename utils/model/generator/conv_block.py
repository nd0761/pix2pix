import torch.nn as nn
from utils.config import TaskConfig

# source shw5


# class ConvBlock(nn.Module):
#     def __init__(self, input_ch, output_ch, kernel_size, block_depth):
#         super().__init__()
#
#         conv_list = [
#             nn.BatchNorm2d(num_features=input_ch),
#             nn.Conv2d(input_ch, output_ch, kernel_size=kernel_size),
#             nn.ReLU()
#         ]
#         for _ in range(block_depth - 1):
#             conv_list.extend(
#                 [nn.BatchNorm2d(num_features=output_ch), nn.Conv2d(output_ch, output_ch, kernel_size=kernel_size),
#                  nn.ReLU()])
#
#         self.conv_net = nn.Sequential(*conv_list)
#
#     def forward(self, x):
#         x = self.conv_net(x)
#         return x
