import torch.nn as nn
import torch
from utils.config import TaskConfig

# source shw5


class DencoderBlock(nn.Module):

    def __init__(self, in_channel, out_channel, norm=True, dropout=None):
        super(DencoderBlock, self).__init__()

        if dropout is not None:
            self.net = nn.Sequential(
                *[
                    nn.ConvTranspose2d(in_channel, out_channel, kernel_size=4, stride=2, padding=1, bias=False),
                    nn.BatchNorm2d(num_features=out_channel),
                    nn.Dropout(dropout),
                    nn.ReLU()
                ]
            )
        else:
            self.net = nn.Sequential(
                *[
                    nn.ConvTranspose2d(in_channel, out_channel, kernel_size=4, stride=2, padding=1, bias=False),
                    nn.BatchNorm2d(num_features=out_channel),
                    nn.ReLU()
                ]
            )

    def forward(self, copied_input, lower_input):
        out = self.net(lower_input)
        out = torch.cat((copied_input, out), 1)
        return out
