import torch.nn as nn
from utils.config import TaskConfig

# source shw5


class EncoderBlock(nn.Module):

    def __init__(self, in_channel, out_channel, norm=True, dropout=0.1):
        super(EncoderBlock, self).__init__()

        if norm:
            self.net = nn.Sequential(
                *[
                    nn.Conv2d(in_channel, out_channel, kernel_size=4, stride=2, padding=1, bias=False),
                    nn.BatchNorm2d(num_features=out_channel),
                    nn.LeakyReLU(0.2)
                ]
            )
        else:
            self.net = nn.Sequential(
                *[
                    nn.Conv2d(in_channel, out_channel, kernel_size=4, stride=2, padding=1, bias=False),
                    nn.LeakyReLU(0.2)
                ]
            )

    def forward(self, x):
        return self.net(x)

