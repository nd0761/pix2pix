import torch
import torch.nn as nn
from utils.config import TaskConfig
from .encoder import EncoderBlock
from .decoder import DencoderBlock


# source shw5


class UNet(nn.Module):
    def __init__(self, config=TaskConfig()):
        super(UNet, self).__init__()

        self.encoder = []
        cur_channel = config.enc_in
        next_channel = config.enc_hidden_channels[0]
        for i in range(len(config.enc_hidden_channels)):
            next_channel = config.enc_hidden_channels[i]
            if i == 0 or i == len(config.enc_hidden_channels) - 1:
                self.encoder.append(EncoderBlock(cur_channel, next_channel, norm=False))
            else:
                self.encoder.append(EncoderBlock(cur_channel, next_channel))
            cur_channel = next_channel
        self.encoder = nn.ModuleList(self.encoder)

        self.decoder = []
        next_channel = config.dec_hidden_channels[0]
        for i in range(len(config.dec_hidden_channels) - 1):
            cur_channel = config.dec_hidden_channels[i]
            next_channel = config.dec_hidden_channels[i + 1] - config.enc_hidden_channels[-i - 2]
            if i <= 2:
                self.decoder.append(DencoderBlock(cur_channel, next_channel, dropout=0.5))
            else:
                self.decoder.append(DencoderBlock(cur_channel, next_channel, dropout=None))

        self.decoder = nn.ModuleList(self.decoder)
        cur_channel = next_channel

        self.output_block = nn.Sequential(
            *[
                nn.ConvTranspose2d(config.dec_hidden_channels[-1], config.n_classes, kernel_size=4, stride=2, padding=1, bias=False),
                nn.Tanh()
            ]
        )

    def forward(self, x):
        enc_outs = [x.clone()]
        for i in range(len(self.encoder) - 1):
            x = self.encoder[i](x)
            enc_outs.append(x)

        x = self.encoder[-1](x)

        for (dec_layer, enc_out) in zip(self.decoder, reversed(enc_outs)):
            x = dec_layer(enc_out, x)
        x = self.output_block(x)
        return x
