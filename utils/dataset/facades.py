import torchvision.transforms as transforms
from torch.utils.data import Dataset
from PIL import Image

import glob
import os

from utils.config import TaskConfig


class FacadesDataset(Dataset):

    def __init__(
            self, dataset_dir,
            transformers_r=None, transformers_l=None,
            split="train", shape=(256, 256), color="rgb"
    ):

        self.dir_path = os.path.join(dataset_dir, split)
        self.all_files = [name for name in os.listdir(self.dir_path)]
        self.all_files = sorted(self.all_files)

        self.shape = shape
        self.color = color

        self.transformers_l = transformers_l
        self.transformers_r = transformers_r

    def __len__(self):
        return len(self.all_files)

    def __getitem__(self, item):

        filename = self.all_files[item]

        img = Image.open(os.path.join(self.dir_path, filename))

        w, h = img.size
        l, r = img.crop((w // 2, 0, w, h)), img.crop((0, 0, w // 2, h))
        if self.transformers_l is not None:
            l = self.transformers_l(l)
        if self.transformers_r is not None:
            r = self.transformers_r(r)
        return l, r
