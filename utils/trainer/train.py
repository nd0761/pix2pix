from utils.config import TaskConfig
from torch.nn.modules.loss import MSELoss, L1Loss

import wandb

import torch
import torch.nn.functional as F

from tqdm import tqdm


def log_wandb_pic(wandb_session, input_img, predicted_img, mode="train", ground_truth_img=None):
    if predicted_img is not None:
        wandb_session.log({
                  mode + ".input_img": wandb.Image(input_img),
                  mode + ".predicted_img": wandb.Image(predicted_img),
                  mode + ".ground_truth_img": wandb.Image(ground_truth_img)})
    else:
        wandb_session.log({
                  mode + ".input_img": wandb.Image(input_img),
                  mode + ".predicted_img": wandb.Image(predicted_img)})


def train_epoch(
        model, opt, loader, scheduler,
        loss_fn, config=TaskConfig(), wandb_session=None
):
    model.train()

    losses = []
    for i, (input_img, ground_truth_img) in tqdm(enumerate(loader), total=len(loader)):
        if config.batch_limit != -1 and i >= config.batch_limit:
            break
        input_img = input_img.to(config.device)
        ground_truth_img = ground_truth_img.to(config.device)

        opt.zero_grad()
        predicted_img = model(input_img)

        loss = loss_fn(ground_truth_img, predicted_img)
        losses.append(loss.detach().cpu().numpy())

        loss.backward()
        opt.step()
        if scheduler is not None:
            scheduler.step()

        if i % config.log_loss_every_iteration == 0 and config.wandb:
            if scheduler is not None:
                a = scheduler.get_last_lr()[0]
                wandb_session.log({
                    "learning_rate": a
                })
            wandb_session.log({
                "train.loss": loss.detach().cpu().numpy()
            })
        if i % config.log_img_every_iteration == 0:
            log_wandb_pic(wandb_session, input_img[0], predicted_img[0], ground_truth_img=ground_truth_img[0])
    return losses


@torch.no_grad()
def validation(
        model, loader, loss_fn,
        config=TaskConfig(), wandb_session=None,
        mode="val"
):
    model.eval()

    val_losses = []
    for i, (input_img, ground_truth_img) in tqdm(enumerate(loader), total=len(loader)):
        input_img = input_img.to(config.device)
        ground_truth_img = ground_truth_img.to(config.device)

        predicted_img = model(input_img)

        loss = loss_fn(ground_truth_img, predicted_img)
        val_losses.append(loss.detach().cpu().numpy())
        if i % config.log_loss_every_iteration == 0 and config.wandb:
            wandb_session.log({
                "val.loss": loss.detach().cpu().numpy()
            })
        if i % config.log_img_every_iteration == 0:
            log_wandb_pic(wandb_session, input_img[0], predicted_img[0], mode=mode, ground_truth_img=ground_truth_img[0])

    return val_losses


def train(
        model, opt,
        train_loader, val_loader,
        scheduler=None,
        save_model=False, model_path=None,
        config=TaskConfig(), wandb_session=None
):
    best_loss = -1.
    criterion = L1Loss()
    for n in range(config.num_epochs):
        #         if config.log_audio and n % config.laep == 0:
        train_losses = train_epoch(
            model, opt, train_loader, scheduler,
            criterion, config, wandb_session)
        train_loss = sum(train_losses) / len(train_losses)
        print("TRAIN LOSS", train_loss)

        if best_loss < 0 or train_loss < best_loss:
            print("UPDATING BEST MODEL, NEW BEST TRAIN_LOSS:", train_loss)
            best_loss = train_loss
            best_model_path = config.model_path + "/best_model"
            torch.save(model.state_dict(), best_model_path)
        if n % config.save_models_every_epoch == 0:
            model_path = config.model_path + "/model_epoch"
            torch.save(model.state_dict(), model_path)

        validation(
            model, val_loader,
            criterion, config, wandb_session
        )

        print('\n------\nEND OF EPOCH', n, "\n------\n")
    if save_model:
        torch.save(model.state_dict(), model_path)
