import torch
import dataclasses


@dataclasses.dataclass
class TaskConfig:
    work_dir: str = "/content/pix2pix"  # pix2pix2 directory
    work_dir_dataset: str = "/content/datasets"  # dataset directory
    model_path: str = "/content/models"  # path to save future models
    dataset_name: str = "facades"  # name of a dataset

    torch_seed: int = 42  # set torch seed for reproduction
    num_epochs: int = 200
    batch_size: int = 1
    batch_limit: int = -1  # set number of batches that will be used in training

    device: torch.device = torch.device(
        'cuda:0' if torch.cuda.is_available() else 'cpu')  # set device

    wandb: bool = True
    wandb_api: str = "99f2c4dae0db3099861ebd92a63e1194f42d16d9"  # wandb api
    wandb_project: str = "pix2pix-checkpoint"  # wandb project name
    log_img_every_iteration: int = 5  # set -1 if you don't want to log any images
    log_loss_every_iteration: int = 5  # set -1 if you don't want to log any data

    save_models_every_epoch: int = 3  # model will be saved every save_models_every_epoch'th epoch

    betas: tuple = (0.5, 0.999)  # Adam betas

    learning_rate: float = 2e-4
    weight_decay: float = 1e-5

    # UNET config

    n_classes: int = 3

    def __init__(self):
        # configs for pix2pix generator  encoder
        self.enc_in = 3
        self.enc_hidden_channels = [64, 128, 256, 512, 512, 512, 512, 512]

        # configs for pix2pix generator  decoder
        self.dec_hidden_channels = [512, 1024, 1024, 1024, 1024, 512, 256, 128]
        self.dec_out = 3

        # configs for pix2pix discriminator
